#include <stdlib.h>
#include <stdio.h>
#include "bmp_io.h"
#include "bmp_struct.h"

uint64_t padding (uint64_t biWidth){
    return biWidth % 4;
}

struct bmp_header create_header (struct image const* img) {
    struct bmp_header header = {};
//    header.bfType = 0x4D42;
    header.bfType = 'MB';
    header.bfileSize = img->width * img->height * sizeof(struct pixel) + img->height * padding(img->width) +
                        sizeof(struct bmp_header);
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = 40;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = header.bfileSize - header.bOffBits;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;
    return header;
}

static enum read_status check_header(struct bmp_header const header){
    if (header.bfType != 'MB') { return READ_INVALID_SIGNATURE; } //header.bfType != 0x4D42
    if (header.biBitCount != 24){ return READ_INVALID_BITS; }
    if (header.biSize !=40
        || header.biCompression!=0
        || header.bfileSize != header.bOffBits + header.biSizeImage){
        return READ_INVALID_HEADER;
    }
    return READ_OK;
}

enum read_status from_bmp( FILE* input, struct image* img){
    if (!input) { return READ_INVALID_PATH; }
    struct bmp_header header = {0};
    fread(&header, 1, sizeof (struct bmp_header), input);
    if (check_header(header) != READ_OK ) return check_header(header);

    const uint64_t data_size = header.biHeight * header.biWidth * sizeof(struct pixel);
    img->data = (struct pixel *) malloc(data_size);
    img->height = header.biHeight;
    img->width = header.biWidth;

    for (size_t row = 0; row < header.biHeight; row++) {
        fread(&(img->data[row * img->width]), sizeof(struct pixel), img->width, input);
        fseek(input, padding(header.biWidth), SEEK_CUR);
    }
    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ){
    struct bmp_header header = create_header(img);

    if(!fwrite(&header, 1, sizeof(struct bmp_header), out)){
        return WRITE_ERROR;
    }
    const uint8_t nulls = {0};
    for (size_t i = 0; i < img->height; i++) {
        if(!fwrite(&img->data[i * img->width], sizeof(struct pixel), img->width, out) ||
        ( padding(img->width)!=0 && !fwrite(&nulls, 1, padding(img->width), out))) {return WRITE_ERROR;}
    }
    return WRITE_OK;
}

static const char* read_descriptions[] = {
        [READ_OK] = "READ_OK",
        [READ_INVALID_SIGNATURE] = "READ_INVALID_SIGNATURE",
        [READ_INVALID_BITS] = "READ_INVALID_BITS",
        [READ_INVALID_HEADER] = "READ_INVALID_HEADER",
        [READ_INVALID_PATH] = "READ_INVALID_PATH",
};

void print_read_status(enum read_status status){
    printf("%s\n", read_descriptions[status]);
}

static const char* write_descriptions[] = {
        [WRITE_OK] = "WRITE_OK",
        [WRITE_ERROR] = "WRITE_ERROR"
};

void print_write_status(enum write_status status){
    printf("%s\n", write_descriptions[status]);
}





