#ifndef FILE_IO_H
#define FILE_IO_H
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

enum open_status {
    OPEN_OK = 0,
    OPEN_ERROR
};
enum open_reason {
    READ = 0,
    WRITE
};

enum close_status {
    CLOSE_OK = 0,
    CLOSE_ERROR
};


enum open_status file_open(FILE** file, const char *filepath, enum open_reason reason);
enum close_status file_close(FILE* file);

void print_open_status (enum open_status status);
void print_close_status (enum close_status status);

#endif //FILE_IO_H
