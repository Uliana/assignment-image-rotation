#include "file_io.h"

enum open_status file_open(FILE** file, const char *filepath, enum open_reason reason) {
    char* modes = "";
    switch (reason) {
        case READ: {
            modes = "rb";
            break;
        }
        case WRITE:{
            modes = "wb";
            break;
        }
    }
    *file = fopen(filepath, modes);
    if (!*file) {
        return OPEN_ERROR;
    }
    return OPEN_OK;
}

enum close_status file_close(FILE* file) {
    fclose(file);
    return CLOSE_OK;
}

static const char* open_descriptions[] = {
        [OPEN_OK] = "OPEN_OK",
        [OPEN_ERROR] = "OPEN_ERROR",
};

void print_open_status (enum open_status status){
    printf("%s\n", open_descriptions[status]);
}

static const char* close_descriptions[] = {
        [CLOSE_OK] = "CLOSE_OK",
        [CLOSE_ERROR] = "CLOSE_ERROR"
};

void print_close_status (enum close_status status){
    printf("%s\n", close_descriptions[status]);
}
