#include <stdbool.h>
#include <stdio.h>

#include "util.h"
#include "file_io.h"
#include "bmp_struct.h"
#include "bmp_io.h"
#include "bmp_rotate.h"

void usage() {
    fprintf(stderr, "Usage: ./image-rotate BMP_INPUT_FILE_NAME BMP_OUTPUT_FILE_NAME\n");
}

int main( int argc, char** argv ) {
    if (argc != 3) usage();
    if (argc < 3) err("Not enough arguments \n" );
    if (argc > 3) err("Too many arguments \n" );

    const char *input_path = argv[1];
    const char *output_path = argv[2];

    struct image img = {};

    FILE* input = NULL;
    enum open_status status_openr = file_open( &input, input_path, READ );
    print_open_status(status_openr);
    if (status_openr){
        return status_openr;
    }
    
    enum read_status status_r = from_bmp(input, &img);
    print_read_status(status_r);
    if (status_r){
        return status_openr;
    }

    file_close(input);

    struct image rotate_img = rotate(img); 

    FILE* output = NULL;
    enum open_status status_openw = file_open(&output, output_path, WRITE);
    if (status_openw){
        return status_openw;
    }

    enum write_status status_w = to_bmp(output, &rotate_img);
    print_write_status(status_w);
    if (status_r){
        return status_openr;
    }

    file_close(output);

    return 0;
}
